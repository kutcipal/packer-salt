john.doe:
  group.present:
    - gid: 4000
  user.present:
    - fullname: john doe
    - shell: /bin/bash
    - home: /home/john.doe
    - uid: 4000
    - gid: 4000
    - groups:
      - wheel

